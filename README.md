# istio-egress

An istio egress gateway configurator with Helm


## Requirements

This chart has been tested in the following environment:

* GKE
* Managed Service Mesh (ASM)

You'll need to have Helm installed in order to use this chart.


## Getting Started

>>>
⚠️ **WARNING**

Only a **SINGLE** instance of this chart is supported per cluster,
please **DO NOT** install multiple copies of this chart.
>>>

First we need to prepare a namespace for all the egress gateway related resources and label it so the workloads from this namespace will belong to the mesh:

```bash
kubectl create ns istio-egress
kubectl label ns istio-egress istio.io/rev=asm-managed
```

Next, prepare a values-override.yaml that matches your setup.
For available fields please refer to [values.yaml](values.yaml) for default values.

Once you have the values-override.yaml ready, run the following to generate the resources and apply them to your cluster:

```bash
helm template ./ -n istio-egress -f values-override.yaml | kubectl apply -n istio-egress -f -
```

Observe the resources inside namespace `istio-egress`, once everything's ready, you're good to go!
